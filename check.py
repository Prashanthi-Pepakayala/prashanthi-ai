from tkinter import *
from tkinter import messagebox
import sqlite3
from PIL import Image, ImageTk

class Home:
    def login_window(self):
        username = self.user_name_input_area.get()
        password = self.user_password_entry_area.get()

        conn = sqlite3.connect('user_credentials.db')
        c = conn.cursor()

        c.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password))
        result = c.fetchone()

        if result:
            messagebox.showinfo("Success", "Login Successful!")
            self.clear_widgets()
            login = Login(self.root)
        else:
            messagebox.showerror("Error", "Invalid Username or Password. Please create an account first.", icon='error')

        conn.commit()
        conn.close()

    def signup_window(self):
        self.clear_widgets()
        signup = Signup(self.root)
    
    def open_contribute_window(self):
        contribute_window = Toplevel(self.root)
        Contribute(contribute_window)

    def clear_widgets(self):
        self.title.destroy()
        self.user_name.destroy()
        self.user_password.destroy()
        self.user_create.destroy()
        self.login_button.destroy()
        self.signup_button.destroy()
        self.user_name_input_area.destroy()
        self.user_password_entry_area.destroy()

    def __init__(self, root):
        self.root = root

        self.title = Label(root, text='WORKVISTA', font=('lucida handwriting', 15))
        self.title.place(x=175, y=35)

        self.user_name = Label(root, text="Username")
        self.user_name.place(x=100, y=110)

        self.user_password = Label(root, text="Password")
        self.user_password.place(x=100, y=150)

        self.user_create = Label(root, text='GO-->', bg='lavender')
        self.user_create.place(x=230, y=223)

        self.login_button = Button(root, text="Login", command=self.login_window)
        self.login_button.place(x=325, y=190)

        self.signup_button = Button(root, text="Signup", command=self.signup_window)
        self.signup_button.place(x=325, y=220)

        self.user_name_input_area = Entry(root, width=30)
        self.user_name_input_area.place(x=170, y=110)

        self.user_password_entry_area = Entry(root, width=30)
        self.user_password_entry_area.place(x=170, y=150)

class Signup:
    def signup(self):
        username = self.user_entry.get()
        password = self.passentry.get()
        profession = self.phone_ent.get()
        email = self.mail_ent.get()

        conn = sqlite3.connect('user_credentials.db')
        c = conn.cursor()

        c.execute("INSERT INTO users (username, password, profession, email) VALUES (?, ?, ?, ?)", (username, password, profession, email))
        conn.commit()
        conn.close()

        messagebox.showinfo("Success", "Account created successfully!")

    def back_to_home(self):
        self.root.destroy()
        home = Home(Tk())
        home.root.mainloop()

    def __init__(self, root):
        self.root = root
        self.root.title("Sign up")
        self.user_name = Label(root, text="USER NAME", font=("Courier New", 10, 'bold'), width=20, justify='left')
        self.user_name.place(x=0, y=110)
        self.user_entry = Entry(root, font=("Courier New", 15, "bold"), width=20, justify="center")
        self.user_entry.place(x=170, y=110)
        self.password = Label(root, text="PASSWORD", font=("Courier New", 10, "bold"), width=20)
        self.password.place(x=0, y=150)
        self.passentry = Entry(root, font=("Courier New", 15, "bold"))
        self.passentry.place(x=170, y=150)
        self.repassword = Label(root, text="Re-enter PASSWORD", font=("Courier New", 10, "bold"))
        self.repassword.place(x=10, y=190)
        self.repassentry = Entry(root, font=("Courier New", 15, "bold"))
        self.repassentry.place(x=170, y=190)
        self.phone_num = Label(root, text="PROFESSION", font=("Courier New", 10, "bold"))
        self.phone_num.place(x=50, y=230)
        self.phone_ent = Entry(root, font=("Courier New", 15, "bold"))
        self.phone_ent.place(x=170, y=230)
        self.mail = Label(root, text="E-MAIL", font=("Courier New", 10, "bold"))
        self.mail.place(x=50, y=270)
        self.mail_ent = Entry(root, font=("Courier New", 15, "bold"))
        self.mail_ent.place(x=170, y=270)
        self.signup_button = Button(root, text="Complete Signup", command=self.signup)
        self.signup_button.place(x=350, y=300)
        self.back_button= Button(root,text="back",command=self.back_to_home )
        self.back_button.place(x=10,y=10)

class Login:
    def open_search_window(self):
        search_window = Toplevel(self.root)
        Search(search_window)

    def open_contribute_window(self):
        contribute_window = Toplevel(self.root)
        Contribute(contribute_window)

    def __init__(self, root):
        self.root = root
        self.root.title("LOGIN SUCCESSFUL")

        self.bt1 = Button(root, text="TO HIRE", command=self.To_Hire_window, bg='pink')
        self.bt1.place(x=75, y=150)

        self.bt2 = Button(root, text="TO CONTRIBUTE", command=self.open_contribute_window, bg='pink')
        self.bt2.place(x=186, y=150)

        self.bt3 = Button(root, text="SEARCH", command=self.open_search_window, bg='pink')
        self.bt3.place(x=350, y=150)

        self.previous_button = Button(root, text="Logout", command=self.back_to_home)
        self.previous_button.place(x=350, y=300)

    def To_Hire_window(self):
        hire = To_Hire(self.root)

    def back_to_home(self):
        self.root.destroy()
        home = Home(Tk())
        home.root.mainloop()

class Contribute:
    def __init__(self, root):
        print("Opening Contribute window")
        self.root = root
        self.root.title("Contribute")
    
class Search:
    def __init__(self, root):
        self.root = root
        self.root.title("Search")

        # Fullscreen mode
        width = self.root.winfo_screenwidth()
        height = self.root.winfo_screenheight()
        self.root.geometry(f"{width}x{height}+0+0")

        # Calculate the center coordinates
        center_x = width // 2
        center_y = height // 2

        # Create and place buttons
        self.btn1 = Button(root, text="Courses", command=self.open_courses_window, bg='pink')
        self.btn1.place(x=center_x - 150, y=center_y - 100)

        self.btn4 = Button(root, text="Projects", command=self.open_projects_window, bg='pink')
        self.btn4.place(x=center_x + 150, y=center_y - 100)

        self.previous_button = Button(root, text="Logout", command=self.back_to_home)
        self.previous_button.place(x=center_x - 0, y=center_y + 50)

    def open_courses_window(self):
        courses_window = Toplevel(self.root)
        Courses(courses_window)

    def open_projects_window(self):
        projects_window = Toplevel(self.root)
        Projects(projects_window)

    def back_to_home(self):
        self.root.destroy()
        home = Home(Tk())
        home.root.mainloop()

class To_Hire:
    def __init__(self, root):
        self.root = root
        self.root.title("To Hire")

        self.btn1 = Button(root, text="Data Scientist", command=self.open_data_scientist_window, bg='pink')
        self.btn1.place(x=75, y=150)

        self.btn2 = Button(root, text=" Web Developer ", command=self.open_web_developer_window, bg='pink')
        self.btn2.place(x=186, y=150)

        self.btn3 = Button(root, text="Software Developer", command=self.open_software_developer_window, bg='pink')
        self.btn3.place(x=295, y=150)

        self.previous_button = Button(root, text="Logout", command=self.back_to_home)
        self.previous_button.place(x=350, y=300)

    def open_data_scientist_window(self):
        data_scientist_window = Toplevel(self.root)
        DataScientist(data_scientist_window)

    def open_web_developer_window(self):
        web_developer_window = Toplevel(self.root)
        WebDeveloper(web_developer_window)

    def open_software_developer_window(self):
        software_developer_window = Toplevel(self.root)
        SoftwareDeveloper(software_developer_window)

    def back_to_home(self):
        self.root.destroy()
        home = Home(Tk())
        home.root.mainloop()

class DataScientist:
    def __init__(self, root):
        self.root = root
        self.root.title("Data Scientist")

class WebDeveloper:
    def __init__(self, root):
        self.root = root
        self.root.title("Web Developer")

class SoftwareDeveloper:
    def __init__(self, root):
        self.root = root
        self.root.title("Software Developer")

class Courses:
    def __init__(self, root):
        self.root = root
        self.root.title("Courses")

class Projects:
    def __init__(self, root):
        self.root = root
        self.root.title("Projects")

# Create a database table to store user credentials
conn = sqlite3.connect('user_credentials.db')
c = conn.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS users (
                id INTEGER PRIMARY KEY,
                username TEXT NOT NULL,
                password TEXT NOT NULL,
                profession TEXT,
                email TEXT
            )''')
conn.commit()
conn.close()

root = Tk()
root.geometry("1550x800+0+0")
root.title("PG1")

home = Home(root)

root.mainloop()
